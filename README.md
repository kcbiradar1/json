# JSON
JSON stands for Javascript Object Notation

JSON is a text format for storing and transporting data.

JSON stores data in name-value pairs and it separated by commas.

In JSON data curly braces hold objects and square braces holds arrays.

Example for json
```json
    '{"name" : "xyz" , "age" : 23, "isStudent": true}'
```
In the above example object is having three properties.
  
    - name
    - age
    - isStudent


The JSON format quite similar to the javascript objects. So JavaScript programs can easily convert JSON and viceversa.

JavaScript has a built in function for converting JSON strings into JavaScript Objects using ```JSON.parse()```

JavaScript also has a built in function for converting an object into a JSON string using ```JSON.stringify()```

In JSON, keys must be strings, written with double quotes. But in javascript object keys can be strings, numbers, or identifier names.

JSON values must be one of the follwing:
- string
- number
- object
- array
- boolean
- null

But in JavaScript it contains all above and also includes some other
- function
- date
- undefined

We can provide date and function as string type later we can convert it.

We can perform the different operation on JSON data, some of listed below:
```json
    json_data = '["hello" , "hi", "Bye"]';

    console.log(json_data[0])  // output : hello

    for(let each_element of json_data) {
        console.log(each_element);
    }
    // output:
    // hello
    // hi
    // bye
```








